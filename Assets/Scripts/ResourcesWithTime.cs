using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourcesWithTime : MonoBehaviour
{
    [SerializeField] private float Resources;
    [SerializeField] private TextMeshProUGUI ResourcesGUI;

    private void Update()
    {
        Resources += Time.deltaTime ;
        ResourcesGUI.text = ":" + Resources.ToString();
    }

}
