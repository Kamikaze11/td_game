using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class MouseClick : MonoBehaviour
{
    public static MouseClickedEvent opened = new MouseClickedEvent();

    private Renderer _renderer;
    [SerializeField] private Image BuyScreen;
    [SerializeField] private bool Open = false;
    [SerializeField] private Button Building1;


    void Start()
    {
        opened.AddListener(onOtherOpened);
        _renderer = GetComponent<Renderer>();
    }

    private void OnMouseDown()
    {
        Debug.Log("click!");
        if (Open == false)
        {
            BuyScreen.gameObject.SetActive(true);
            Open = true;
            _renderer.material.color = Color.blue;
            opened?.Invoke(this);
        }
        else if (Open == true)
        {
            BuyScreen.gameObject.SetActive(false);
            Open = false;
            _renderer.material.color = Color.white;
        }
    }

    public void onOtherOpened(MouseClick mouseClick)
    {
        if (mouseClick != this)
        {
            Open = false;
            _renderer.material.color = Color.white;
        }
    }

}

public class MouseClickedEvent : UnityEvent<MouseClick>
{
}