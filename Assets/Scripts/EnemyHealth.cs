using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] public float Health = 20f;

    public void TakeDamage(int amount)
    {
        Health -= amount;
        if (Health == 0)
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (Health < 0)
        {
            Destroy(gameObject);
        }
    }

}