using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PoisenEf : MonoBehaviour
{
    [SerializeField] private float PoisenStrength = 20;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<NavMeshAgent>().speed = Random.Range(0.5f, 1f);
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<NavMeshAgent>().speed = 3.5f;

        }
    }
    private void OnTriggerStay(Collider other)
    {
        other.GetComponent<EnemyHealth>().Health -= Time.deltaTime;
        return;
    }
}


