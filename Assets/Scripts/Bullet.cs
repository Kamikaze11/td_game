using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 70f;
    public EnemyHealth enemyHealth;
    private Transform target;
    private Vector3 lastPos;
    bool isFirstFrame = true;

    public void seek(Transform _target)
    {
        target = _target;
    }
    private void Update()
    {
        if (isFirstFrame)
        {
            isFirstFrame = false;
            lastPos = transform.position;
            return;
        }
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector3 dir = target.position - transform.position;
        float disanceThisFrame = speed * Time.deltaTime;
        //if (dir.magnitude <= disanceThisFrame)
        //{
        //    HitTarget();
        //    return;
        //}
        transform.Translate(dir.normalized * disanceThisFrame, Space.World);
        //Vector3 offset = (transform.position - lastPos).normalized;
        Debug.DrawLine(transform.position, lastPos, Color.red);
        Ray ray = new Ray(lastPos, transform.position - lastPos);
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Raycast(ray, out hitInfo, (lastPos - transform.position).magnitude))
        {
            EnemyHealth enemyHealth = hitInfo.transform.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(20);
                HitTarget();

            }
        }

        lastPos = transform.position;

    }
    public void HitTarget()
    {
        Destroy(gameObject);
    }




}