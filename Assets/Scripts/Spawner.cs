using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject Enemy;
    [SerializeField] private float Timer;
    [SerializeField] private Vector3 EnemyRot;
    [SerializeField] private float Wave1;
    void Update()
    {
        Timer += Time.deltaTime;
        while (Timer > 1f)
        {
            Instantiate(Enemy, transform.position, transform.rotation );
            Timer = 0;
            ++ Wave1;
        }
        if (Wave1 == 50f)
        {
            Time.timeScale = 0f;
        }
      
    }
}
