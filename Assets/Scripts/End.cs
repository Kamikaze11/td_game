using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class End : MonoBehaviour
{
    [SerializeField] private float EnemyNum;
    [SerializeField] private TextMeshProUGUI GameOver;
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        ++EnemyNum;
    }

    private void Update()
    {
        if (EnemyNum == 50)
        {
            GameOver.enabled = true;
        }
    }

}
